package com.example.maestro123.ormtest;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.maestro123.ormtest.model.User;
import com.example.maestro123.ormtest.orm.OrmBuilder;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Database database = new Database(this, OrmBuilder.generateTable(User.class));
        SQLiteDatabase sql = database.getWritableDatabase();

        User created = new User();
        created.setEmail("testEmail");
        created.setName("testName");
        created.setText("testText");
        created.setTestObject(User.TestEnum.TEST);

        sql.delete(User.class.getSimpleName(), null, null);

        Cursor cursor = sql.query(User.class.getSimpleName(), null, null, null, null, null, null, null);
        Log.e(TAG, "cursor before test: " + cursor.getCount());

        OrmBuilder.insert(created, sql);
        OrmBuilder.insert(created, sql);
        created.setTestObject(null);
        OrmBuilder.insert(created, sql);

        List<User> items = OrmBuilder.load(User.class, sql);

        for (User user : items){
            Log.e(TAG, "loaded user: " + user);
        }

        cursor = sql.query(User.class.getSimpleName(), null, null, null, null, null, null);
        Log.e(TAG, "cursor after test: " + cursor.getCount());

        sql.delete(User.class.getSimpleName(), null, null);

        ContentValues values = new ContentValues();
        values.put("name", created.getName());
        values.put("email", created.getEmail());

        sql.insert(User.class.getSimpleName(), null, values);

        cursor = sql.query(User.class.getSimpleName(), null, null, null, null, null, null, null);
        Log.e(TAG, "cursor count: " + cursor.getCount());

        User user = OrmBuilder.load(User.class, cursor);
        Log.e(TAG, user.toString());

    }
}
