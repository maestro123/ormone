package com.example.maestro123.ormtest.orm;

/**
 * Created by maestro123 on 4/16/16.
 */
public class OrmTable {

    private String mName;
    private String mCreateString;

    public OrmTable(String name, String createString) {
        mName = name;
        mCreateString = createString;
    }

    public String getName() {
        return mName;
    }

    public String getCreateString() {
        return mCreateString;
    }

    @Override
    public String toString() {
        return OrmTable.class.getSimpleName() + "@" + Integer.toHexString(hashCode())
                + ": name = " + mName + ", create string = " + mCreateString;
    }
}
