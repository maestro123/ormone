package com.example.maestro123.ormtest.orm;

/**
 * Created by maestro123 on 4/16/16.
 */
public @interface DataRow {
    boolean isPrimary() default false;

    boolean isAutoIncrement() default false;

    boolean isNotNullable() default false;

    String name() default "";
}
