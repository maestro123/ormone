package com.example.maestro123.ormtest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.maestro123.ormtest.orm.OrmTable;

/**
 * Created by maestro123 on 4/16/16.
 */
public class Database extends SQLiteOpenHelper {

    public static final String TAG = Database.class.getSimpleName();

    private static final String DB_NAME = "TestDb";
    private static final int DB_VERSION = 2;

    private OrmTable[] mTables;

    public Database(Context context, OrmTable... tables) {
        super(context, DB_NAME, null, DB_VERSION);
        Log.e(TAG, "onInit");
        mTables = tables;
        getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e(TAG, "onCreate");
        if (mTables != null) {
            for (OrmTable table : mTables) {
                Log.e(TAG, "create: " + table);
                db.execSQL(table.getCreateString());
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e(TAG, "onUpgrade");
        if (mTables != null) {
            for (OrmTable table : mTables) {
                db.execSQL("DROP TABLE IF EXISTS " + table.getName());
            }
        }
    }

}
