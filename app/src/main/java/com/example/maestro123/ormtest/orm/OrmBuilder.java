package com.example.maestro123.ormtest.orm;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maestro123 on 4/16/16.
 */
public class OrmBuilder {

    public static final String TAG = OrmBuilder.class.getSimpleName();

    private static String tableDef = "create table %s (%s);";

    private static final String ROW_TEXT = "TEXT";
    private static final String ROW_INTEGER = "INTEGER";
    private static final String ROW_LONG = ROW_INTEGER;
    private static final String ROW_BOOLEAN = "BYTE";

    private static final String PRIMARY = "PRIMARY KEY";
    private static final String AUTO_INCREMENT = "AUTOINCREMENT";
    private static final String NOT_NULL = "NOT NULL";

    private static final String INSERT = "INSERT INTO %s (%s) VALUES (%s)";

    private static final int INVALID_POSITION = -1;

    public static final List<OrmTable> generate(Class... classes) {
        ArrayList<OrmTable> tables = new ArrayList<>();
        for (Class cls : classes) {
            OrmTable table = generateTable(cls);
        }
        return tables;
    }

    public static final OrmTable generateTable(Class cls) {
        Field[] fields = cls.getDeclaredFields();
        StringBuilder builder = new StringBuilder();
        for (Field field : fields) {
            String row = makeRow(field);
            if (row != null) {
                if (builder.length() > 0) {
                    builder.append(',');
                }
                builder.append(row);
            }
        }
        return new OrmTable(getTableName(cls), String.format(tableDef, getTableName(cls), builder));
    }

    private static String makeRow(Field field) {
        if (!isAcceptableField(field)) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        Class<?> type = field.getType();

        builder.append(field.getName()).append(' ');

        if (isString(type) || isEnum(type)) {
            builder.append(ROW_TEXT);
        } else if (isInteger(type)) {
            builder.append(ROW_INTEGER);
        } else if (isLong(type)) {
            builder.append(ROW_LONG);
        } else if (isBoolean(type)) {
            builder.append(ROW_BOOLEAN);
        }

        verifyAnnotation(field, builder);

        return builder.toString();
    }

    private static void verifyAnnotation(Field field, StringBuilder builder) {
        DataRow dataRow = field.getAnnotation(DataRow.class);
        if (dataRow != null) {
            if (dataRow.isPrimary()) {
                builder.append(' ').append(PRIMARY);
            }
            if (dataRow.isAutoIncrement()) {
                builder.append(' ').append(AUTO_INCREMENT);
            }
            if (dataRow.isNotNullable()) {
                builder.append(' ').append(NOT_NULL);
            }
        }
    }

    public static int insert(Object origin, SQLiteDatabase database) {
        try {
            Field[] fields = origin.getClass().getDeclaredFields();
            String tableName = getTableName(origin.getClass());
            StringBuilder rowBuilder = new StringBuilder();
            StringBuilder valueBuilder = new StringBuilder();
            for (Field field : fields) {
                if (isAcceptableField(field) && !isAutoIncrement(field)) {
                    if (rowBuilder.length() > 0) {
                        rowBuilder.append(',');
                    }
                    rowBuilder.append(field.getName());
                    if (valueBuilder.length() > 0) {
                        valueBuilder.append(',');
                    }
                    Class<?> type = field.getType();
                    field.setAccessible(true);
                    Object value = field.get(origin);
                    Log.e(TAG, "value: " + value);
                    if (value == null) {
                        Log.e(TAG, "found null value");
                        valueBuilder.append("null");
                        continue;
                    }
                    if (isString(type) || isEnum(type)) {
                        valueBuilder
                                .append('\'')
                                .append(field.get(origin))
                                .append('\'');
                    } else if (isInteger(type)) {
                        valueBuilder.append(field.getInt(origin));
                    } else if (isLong(type)) {
                        valueBuilder.append(field.getLong(origin));
                    } else if (isBoolean(type)) {
                        valueBuilder.append(field.getBoolean(origin) ? 1 : 0);
                    }
                }
            }

            database.beginTransaction();

            Log.e(TAG, "string looks like: " + String.format(INSERT, tableName, rowBuilder, valueBuilder));

            SQLiteStatement statement = database.compileStatement(String.format(INSERT, tableName, rowBuilder, valueBuilder));

            statement.execute();
            statement.close();

            database.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (database.inTransaction()) {
                database.endTransaction();
            }
        }
        return -1;
    }

    public static <T> List<T> load(Class originClass, SQLiteDatabase database) {
        Cursor cursor = database.query(getTableName(originClass), null, null, null, null, null, null);
        ArrayList<T> items = new ArrayList<>();
        while (cursor.moveToNext()) {
            items.add((T) load(originClass, cursor));
        }
        return items;
    }

    public static <T> T load(Class originClass, Cursor cursor) {
        if (cursor == null || cursor.getCount() == 0) {
            return null;
        }
        if (cursor.getPosition() == -1) {
            cursor.moveToFirst();
        }
        try {
            Class<?> cls = Class.forName(originClass.getCanonicalName());
            Object object = cls.newInstance();
            Field[] fields = cls.getDeclaredFields();
            for (Field field : fields) {
                if (isAcceptableField(field)) {
                    updateField(object, field, cursor);
                }
            }
            return (T) object;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void updateField(Object cls, Field field, Cursor cursor) throws IllegalAccessException {
        int columnIndex = cursor.getColumnIndex(field.getName());
        if (columnIndex != -1) {
            Class<?> type = field.getType();
            field.setAccessible(true);
            if (isString(type)) {
                field.set(cls, cursor.getString(columnIndex));
            } else if (isInteger(type)) {
                field.set(cls, cursor.getInt(columnIndex));
            } else if (isLong(type)) {
                field.set(cls, cursor.getLong(columnIndex));
            } else if (isBoolean(type)) {
                field.set(cls, cursor.getInt(columnIndex) == 1);
            } else if (isEnum(type)) {
                if (!cursor.isNull(columnIndex)) {
                    field.set(cls, Enum.valueOf((Class<Enum>) type, cursor.getString(columnIndex)));
                }
            }
        }
    }

    private static String getTableName(Class cls) {
        DataTable table = (DataTable) cls.getAnnotation(DataTable.class);
        String name = null;
        if (table != null) {
            name = table.name();
        }
        if (TextUtils.isEmpty(name)) {
            name = cls.getSimpleName();
        }
        return name;
    }

    private static boolean isAcceptableField(Field field) {
        return !field.isSynthetic();
    }

    private static boolean isAutoIncrement(Field field) {
        DataRow row = field.getAnnotation(DataRow.class);
        if (row != null) {
            return row.isAutoIncrement();
        }
        return false;
    }

    private static boolean isString(Class<?> type) {
        return type.equals(String.class);
    }

    private static boolean isInteger(Class<?> type) {
        return type.equals(Integer.class) || type.equals(Integer.TYPE);
    }

    private static boolean isLong(Class<?> type) {
        return type.equals(Long.class) || type.equals(Long.TYPE);
    }

    private static boolean isBoolean(Class<?> type) {
        return type.equals(Boolean.class) || type.equals(Boolean.TYPE);
    }

    private static boolean isEnum(Class<?> type) {
        return type.isEnum();
    }

}
