package com.example.maestro123.ormtest.orm;

/**
 * Created by maestro123 on 4/16/16.
 */
public @interface DataTable {

    String name() default "";

}
