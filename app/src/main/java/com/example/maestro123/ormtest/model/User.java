package com.example.maestro123.ormtest.model;

import com.example.maestro123.ormtest.orm.DataRow;

/**
 * Created by maestro123 on 4/16/16.
 */
public class User {

    @DataRow(isPrimary = true, isAutoIncrement = true)
    private long id;

    private String name;

    private String email;

    private String text;

    @DataRow(isNotNullable = true)
    private TestEnum testObject;

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTestObject(TestEnum testObject) {
        this.testObject = testObject;
    }

    public TestEnum getTestObject() {
        return testObject;
    }

    public enum TestEnum {
        THIS, IS, TEST
    }

    @Override
    public String toString() {
        return "User[" + Integer.toHexString(hashCode()) + "]: name = " + name
                + ", id = " + id + ", email = " + email + ", text = " + text
                + ", testObject = " + testObject;
    }
}
